<?php

namespace App;

use App\Role;
use Illuminate\Database\Eloquent\Model;
use illuminate\Support\Str;

class User extends Model
{
    protected $fillable = ['name','email','username','role_id' ];

    protected $PrimaryKey ='id';

    protected $KeyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = str::uuid();
            }
            $model->role_id =Role::where('name', 'author')->first()->id;
        });
    }

    public function role(){

        return $this->belongsTo('App\Role');
    }
}
