<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;


class PostController extends Controller
{
    public function index(){
        $post =Post::latest()->get();
        
        return response()->json([
            'succes' => true,
            'message'=> 'list data post',
            'data' => $post
            
        ],200);
    }
}
