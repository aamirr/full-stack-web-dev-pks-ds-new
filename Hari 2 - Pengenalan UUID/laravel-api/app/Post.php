<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\support\str;

class Post extends Model
{
    protected $fillable = ['title','description'];

    protected $PrimaryKey ='id';

    protected $KeyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = str::uuid();
            }

        });
}
}
