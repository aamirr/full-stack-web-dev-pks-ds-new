<?php

namespace App;
use illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];

    protected $PrimaryKey ='id';

    protected $KeyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = str::uuid();
            }
        });
    }

    public function users()
    {

        return $this->hasMany('App\User');
    }

}
