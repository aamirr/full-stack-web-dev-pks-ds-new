//soal 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

//jawaban 1
//rumus luas persegi panjang

// let luaspersegipanjang = function(p,l){
//     return p*l;
// };
// console.log('function =',luaspersegipanjang(10,8));

// let luas =(p,l) => {return p*l;};
// console.log('arrow=',luas(10,8));

//rumus luas segitiga

// let luassegitiga = function(a,t,s){
//     return a*t/s;
// };
// console.log('function =',luassegitiga(12,4,2));
// let luas =(a,t,s) => {return a*t/s};
// console.log =('arrow=',luas(12,4,2));

//soal2
//Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//       }
//     }
//   }
   
//   //Driver Code 
//   newFunction("William", "Imoh").fullName() 

//jawaban2

// const firstName ='wilam'
// const lastName ='imoh'

// const thestring =`${firstName} ${lastName}`

// console.log(thestring)

//soal 3
// Diberikan sebuah objek sebagai berikut:
// const newObject = {
//   firstName: "Muhammad",
//   lastName: "Iqbal Mubarok",
//   address: "Jalan Ranamanyar",
//   hobby: "playing football",
// }
// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
// // Driver code
// console.log(firstName, lastName, address, hobby)


//jawaban3
// var trainer ={
//     firstName:'muhamd',
//     lastname:'iqbal mubarok',
//     address : 'jalan ranamayar',
//     hobby:'playing footbal',
// };
// const{firstName,lastname,address,hobby}=trainer
// console.log(trainer)

// soal 4

// Kombinasikan dua array berikut menggunakan array spreading ES6
// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)

//soal5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

// const planet = "earth" 
// const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

//jawaban 5
const firstName ='lorem'
const view ='glass'
const lastname ='dolor site amet'
const elit = 'consectetur adipiscing elit'
const planet ='planet'

const thestring =`${firstName} ${view} ${lastname} ${elit} ${planet}`
console.log(thestring)